module.exports = {
  devServer: {
    disableHostCheck: true
  },
  transpileDependencies: [/node_modules[/\\\\]vuetify[/\\\\]/],
  lintOnSave: false
}
