import Vue from 'vue'
import Router from 'vue-router'
import Home from '../pages/Home'
import About from '../pages/About'
import Cookies from '../pages/Cookies'
import Event from '../pages/Event'
import EventRegistrations from '../pages/EventRegistrations'
import NotFound from '../pages/NotFound'
import Dashboard from '../pages/admin/Dashboard'
import AdminEventsIndex from '../pages/admin/events/AdminEventsIndex'
import AdminEventsNew from '../pages/admin/events/AdminEventsNew'
import AdminEventsEdit from '../pages/admin/events/AdminEventsEdit'
import AdminFieldsIndex from '../pages/admin/fields/AdminFieldsIndex'
import AdminFieldsNew from '../pages/admin/fields/AdminFieldsNew'
import AdminFieldsEdit from '../pages/admin/fields/AdminFieldsEdit'
import AdminRegistrationsIndex from '../pages/admin/registrations/AdminRegistrationsIndex'
import AdminRegistrationsView from '../pages/admin/registrations/AdminRegistrationsView'
import Login from '../pages/Login'
import Signup from '../pages/Signup'
import UserProfile from '../pages/UserProfile'
import ForgotPassword from '../pages/ForgotPassword'
import AdminUsersIndex from '../pages/admin/users/AdminUsersIndex'
import AdminAccessDenied from '../pages/admin/AccessDenied'
import Store from '../store/index'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [{
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/cookies',
    name: 'Cookies',
    compoment: Cookies
  },
  // Auth
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/signup',
    name: 'Signup',
    component: Signup
  },
  {
    path: '/profile',
    name: 'User Profile',
    component: UserProfile
  },
  {
    path: '/forgot-password',
    name: 'Forgot Password',
    component: ForgotPassword
  },
  // 404 page
  {
    path: '/404',
    name: '404',
    component: NotFound
  },
  // Admin
  {
    path: '/admin',
    component: Dashboard,
    children: [{
      path: 'events',
      name: 'AdminEventsIndex',
      component: AdminEventsIndex
    },
    {
      path: 'events/new',
      name: 'AdminEventsNew',
      component: AdminEventsNew,
      meta: {
        requiresSuperAdmin: true
      }
    },
    {
      path: 'events/:slug/edit',
      name: 'AdminEventsEdit',
      component: AdminEventsEdit,
      props: true,
      meta: {
        requiresSuperAdmin: true
      }
    },
    {
      path: 'fields',
      name: 'AdminFieldsIndex',
      component: AdminFieldsIndex
    },
    {
      path: 'fields/:id/edit',
      name: 'AdminFieldsEdit',
      component: AdminFieldsEdit
    },
    {
      path: 'fields/new',
      name: 'AdminFieldsNew',
      component: AdminFieldsNew
    },
    {
      path: 'registrations',
      name: 'AdminRegistrationsIndex',
      component: AdminRegistrationsIndex
    },
    {
      path: 'registrations/:slug',
      name: 'AdminRegistrationsView',
      props: true,
      component: AdminRegistrationsView,
      beforeEnter (to, from, next) {
        Store.dispatch('getEvents').then(() => {
          let res = Store.state.events.filter(ev => {
            return ev.slug === to.params.slug
          })

          if (res.length) {
            next()
          } else {
            Store.dispatch('sendNotification', {
              snackbar: true,
              timeout: 4000,
              status: 'error',
              message: 'Event is closed!'
            })
            next('/404')
          }
        })
      }
    },
    {
      path: 'users',
      name: 'AdminUsersIndex',
      component: AdminUsersIndex,
      meta: {
        requiresSuperAdmin: true
      }
    },
    {
      path: 'accessdenied',
      name: 'AdminAccessDenied',
      component: AdminAccessDenied
    }
    ],
    meta: {
      requiresAuth: true
    }
  },
  // Events
  {
    path: '/:slug',
    name: 'Event',
    component: Event,
    beforeEnter (to, from, next) {
      Store.dispatch('getEvents').then(() => {
        let res = Store.getters.getEventsOpen.filter(ev => {
          return ev.slug === to.params.slug
        })

        if (res.length) {
          next()
        } else {
          Store.dispatch('sendNotification', {
            snackbar: true,
            timeout: 4000,
            status: 'error',
            message: 'Event is closed!'
          })
          next('/404')
        }
      })
    }
  },
  {
    path: '/:slug/registrations',
    name: 'EventRegistrations',
    component: EventRegistrations
  },
  // Errors
  {
    path: '*',
    redirect: '/404'
  }
  ]
})
