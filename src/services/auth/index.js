import axios from 'axios'

export default {
  login ({ email, password }) {
    return axios.post('/login', {
      email, password
    })
      .then(response => {
        if (response.data && response.data.token) {
          return {
            status: 'success',
            token: 'Bearer ' + response.data.token
          }
        } else if (response && response.error) {
          return {
            status: 'error',
            error: response.error
          }
        } else return {}
      })
  },

  signup ({ user, password }) {
    return axios.post(`/signup`, {
      user, password
    })
      .then(response => {
        if (response.message && !response.error) {
          return {
            status: 'success',
            message: response.message
          }
        } else if (response.error) {
          const errorMessage = response.error.errors && response.error.errors.length &&
          response.error.errors[0].message

          return {
            status: 'error',
            error: errorMessage || ''
          }
        }
        return response
      })
  }
}
