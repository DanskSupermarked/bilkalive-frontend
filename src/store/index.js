import Vue from 'vue'
import Vuex from 'vuex'
import 'es6-promise/auto'
import StoresApi from '@/services/api/Stores'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

const checkEmail = function (email) {
  let emailNormalized = email.trim().toLowerCase()

  return emailNormalized.endsWith('@sprintingsoftware.com') ||
  emailNormalized.endsWith('@ontarget-group.com') ||
  emailNormalized.endsWith('@bilka.dk') ||
  emailNormalized.endsWith('@foetex.dk') ||
  emailNormalized.endsWith('@salling.dk') ||
  emailNormalized.endsWith('@sallinggroup.com') ||
  emailNormalized.endsWith('@netto.dk')
}

const actions = {
  clearError ({ commit }) {
    commit('setAuthError', '')
  },

  async deleteAccount ({ commit }, { email, password }) {
    let user = firebase.auth().currentUser
    let credential = firebase.auth.EmailAuthProvider.credential(email, password)
    await firebase.firestore().collection('users').doc(user.uid).delete()

    await user.reauthenticateWithCredential(credential)
    user.delete()
    commit('setUser', undefined)
    commit('setAuthEmailVerified', false)
  },

  async deleteUser ({ commit }, { userId }) {
    await firebase.firestore().collection('users').doc(userId).delete()

    let users = []
    let querySnapshotAllUsers = await firebase.firestore().collection('users').get()
    querySnapshotAllUsers.forEach(doc => {
      let item = Object.assign({}, doc.data())
      item.id = doc.id
      users.push(item)
    })
    commit('setUsers', users)
  },

  async editUser ({ commit }, { userId, selectedStore }) {
    let updatedInfo = {
      storeId: selectedStore.id,
      storeName: selectedStore.name
    }

    await firebase.firestore().collection('users').doc(userId).update(updatedInfo)

    let users = []
    let querySnapshotAllUsers = await firebase.firestore().collection('users').get()
    querySnapshotAllUsers.forEach(doc => {
      let item = Object.assign({}, doc.data())
      item.id = doc.id
      users.push(item)
    })
    commit('setUsers', users)
  },

  async editUserAdmin ({ commit }, { userId, admin }) {
    let updatedInfo = { admin }

    await firebase.firestore().collection('users').doc(userId).update(updatedInfo)

    let users = []
    let querySnapshotAllUsers = await firebase.firestore().collection('users').get()
    querySnapshotAllUsers.forEach(doc => {
      let item = Object.assign({}, doc.data())
      item.id = doc.id
      users.push(item)
    })
    commit('setUsers', users)
  },

  async getEvents ({ commit }) {
    let events = []
    await firebase.firestore().collection('events').get()
      .then(querySnapshot => {
        querySnapshot.forEach(ev => {
          let item = ev.data()
          item['id'] = ev.id
          events.push(item)
        })
      })
    commit('setEvents', events)
  },

  async getRegistrations ({ commit }) {
    let registrations = []
    await firebase.firestore().collection('registrations').get()
      .then(querySnapshot => {
        querySnapshot.forEach(reg => {
          let item = reg.data()
          item['id'] = reg.id
          registrations.push(item)
        })
      })
    commit('setRegistrations', registrations)
  },

  async getFields ({ commit }) {
    let fields = []
    await firebase.firestore().collection('fields').get()
      .then(querySnapshot => {
        querySnapshot.forEach(field => {
          let item = field.data()
          item['id'] = field.id
          fields.push(item)
        })
      })
    commit('setFields', fields)
  },

  async getStores ({ commit, state }) {
    if (!state.stores.length) {
      let stores = await StoresApi.getStores()
      commit('setStores', stores)
    }
  },

  async login ({ commit }, { email, password }) {
    commit('setAuthError', '')
    commit('setAuthLoading', true)

    try {
      let user = await firebase.auth().signInWithEmailAndPassword(email, password)
      commit('setAuthEmailVerified', user.user.emailVerified)
      commit('setAuthLoading', false)

      let querySnapshot = await firebase.firestore().collection('users').doc(user.user.uid).get()
      commit('setUser', querySnapshot.data())
    } catch (error) {
      commit('setAuthError', error.message)
      commit('setAuthLoading', false)
    }
  },

  async logout ({ commit }) {
    await firebase.auth().signOut()
    commit('setUser', undefined)
    commit('setAuthEmailVerified', false)
  },

  async resendVerificationEmail () {
    let user = firebase.auth().currentUser
    try {
      await user.sendEmailVerification()
    }
    catch (e) {
      alert(e)
    }
  },

  async resetPassword ({ commit }, { email }) {
    commit('setAuthError', '')
    commit('setAuthLoading', true)

    if (!checkEmail(email)) {
      commit('setAuthError', 'Email skal tilhøre et af de tilladte domæner.')
      commit('setAuthLoading', false)
      return
    }

    try {
      await firebase.auth().sendPasswordResetEmail(email)
      commit('setAuthPasswordLinkSent', true)
    } catch (error) {
      if (error.message) {
        commit('setAuthError', error.message)
      } else {
        commit('setAuthError', 'Uforventet fejl')
      }
    } finally {
      commit('setAuthLoading', false)
    }
  },

  async signup ({ commit }, { email, password, passwordRepeat, store }) {
    commit('setAuthError', '')
    commit('setAuthLoading', true)

    if (!checkEmail(email)) {
      commit('setAuthError', 'Email skal tilhøre et af de tilladte domæner.')
      commit('setAuthLoading', false)
      return
    }

    if (password !== passwordRepeat) {
      commit('setAuthError', 'Passwords do not match.')
      commit('setAuthLoading', false)
      return
    }

    try {
      let user = await firebase.auth().createUserWithEmailAndPassword(email, password)

      let newUser = {
        admin: false,
        email: user.user.email,
        storeId: store.id,
        storeName: store.name
      }

      await firebase.firestore().collection('users').doc(user.user.uid).set(newUser)

      await user.user.sendEmailVerification()

      commit('setAuthEmailVerified', user.user.emailVerified)
      commit('setUser', newUser)
    } catch (error) {
      if (error.message) {
        commit('setAuthError', error.message)
      } else {
        commit('setAuthError', 'Uforventet fejl')
      }
    } finally {
      commit('setAuthLoading', false)
    }
  },

  sendNotification ({ commit }, notification) {
    commit('setNotification', notification)
  },

  async clearNotification ({ commit }) {
    let notification = { snackbar: false }
    commit('setNotification', notification)
  }
}

const getters = {
  getAuthEmailVerified: state => {
    return state.authEmailVerified
  },
  getEventBySlug: state => slug => {
    return state.events.find(ev => ev.slug === slug)
  },
  getEventsOpen: state => {
    return state.events.filter(ev => {
      if ((new Date(ev.startDate) <= new Date()) && (new Date(ev.endDate) >= new Date())) {
        return ev
      }
    })
  },
  getEventsSoon: state => {
    return state.events.filter(ev => {
      if (new Date(ev.startDate) > new Date()) {
        return ev
      }
    })
  },
  getEventsEnded: state => {
    return state.events.filter(ev => {
      if (new Date(ev.endDate) < new Date()) {
        return ev
      }
    })
  },
  getUser: state => {
    return state.user
  },
  getRegistrationsForOpenEvents: (state, getters) => {
    let openEventsIds = getters.getEventsOpen.map(el => { return el.id })
    let openEventRegistrations = state.registrations.filter(el => {
      return el.registrationEvent && openEventsIds.includes(el.registrationEvent.id)
    })

    return openEventRegistrations
  },
  // TODO: not sure if that is needed, because registration is closed until Event start
  getRegistrationsForSoonEvents: (state, getters) => {
    let openEventsIds = getters.getEventsSoon.map(el => { return el.id })
    let openEventRegistrations = state.registrations.filter(el => {
      return el.registrationEvent && openEventsIds.includes(el.registrationEvent.id)
    })

    return openEventRegistrations
  },
  getRegistrationsForEndedEvents: (state, getters) => {
    let openEventsIds = getters.getEventsEnded.map(el => { return el.id })
    let openEventRegistrations = state.registrations.filter(el => {
      return el.registrationEvent && openEventsIds.includes(el.registrationEvent.id)
    })

    return openEventRegistrations
  }
}

const mutations = {
  setAuthEmailVerified (state, authEmailVerified) {
    state.authEmailVerified = authEmailVerified
  },
  setAuthError (state, authError) {
    state.authError = authError
  },
  setAuthLoading (state, authLoading) {
    state.authLoading = authLoading
  },
  setAuthPasswordLinkSent (state, authPasswordLinkSent) {
    state.authPasswordLinkSent = authPasswordLinkSent
  },
  setEvents (state, events) {
    state.events = events
  },
  setRegistrations (state, registrations) {
    state.registrations = registrations
  },
  setFields (state, fields) {
    state.fields = fields
  },
  setSelectedStore (state, selectedStore) {
    state.selectedStore = selectedStore
  },
  setStores (state, stores) {
    state.stores = stores
  },
  setUser (state, user) {
    state.user = user
  },
  setUsers (state, users) {
    state.users = users
  },
  setNotification (state, notification) {
    state.notification = notification
  }
}

Vue.use(Vuex)

export default new Vuex.Store({
  actions,
  getters,
  mutations,
  state: {
    authEmailVerified: false,
    authError: '',
    authLoading: false,
    authPasswordLinkSent: false,
    events: [],
    registrations: [],
    fields: [],
    selectedStore: undefined,
    stores: [],
    user: undefined,
    users: [],
    notification: null
  }
})
