export default {
  computed: {
    currentUser () {
      return this.$store.state.user
    },
    currentUserIsAdmin () {
      return this.$store.state.user && (this.$store.state.user.admin === true)
    }
  },
  methods: {
    signout () {
      this.$store.dispatch('logout')
    }
  }
}
