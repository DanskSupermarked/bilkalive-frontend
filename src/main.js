import 'babel-polyfill'
import Vue from 'vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import App from './App.vue'
import router from './router/router'
import store from './store'
import axios from 'axios'
import * as firebase from 'firebase/app'
import 'firebase/auth'
import * as storage from 'firebase/firestore'
import 'vuetify/dist/vuetify.min.css'
import Auth from './mixins/auth'

Vue.use(VueRouter)
Vue.use(Vuetify)
Vue.mixin(Auth)
Vue.config.productionTip = false
Vue.prototype.$http = axios
Vue.prototype.$root_api = process.env.VUE_APP_ROOT_API
Vue.prototype.$app_title = process.env.VUE_APP_TITLE

axios.defaults.baseURL = process.env.VUE_APP_ROOT_API

// Initialize Firebase
var config = {
  apiKey: 'AIzaSyAVOaFJGzse_rCj6U86sRjeAdLyToeOu7M',
  authDomain: 'bilkalive-f90e4.firebaseapp.com',
  databaseURL: 'https://bilkalive-f90e4.firebaseio.com',
  projectId: 'bilkalive-f90e4',
  storageBucket: 'bilkalive-f90e4.appspot.com',
  messagingSenderId: '744586948720'
}

firebase.initializeApp(config)
Vue.prototype.$storage = storage
Vue.prototype.$db = firebase.firestore()

firebase.auth().onAuthStateChanged(async (user) => {
  if (user !== null) {
    const querySnapshot = await firebase.firestore().collection('users').doc(user.uid).get()
    const currentUser = querySnapshot.data()
    store.commit('setAuthEmailVerified', user.emailVerified)
    store.commit('setUser', currentUser)

    if (currentUser.admin) {
      let users = []
      const querySnapshotAllUsers = await firebase.firestore().collection('users').get()
      querySnapshotAllUsers.forEach(doc => {
        let item = Object.assign({}, doc.data())
        item.id = doc.id
        users.push(item)
      })
      store.commit('setUsers', users)
    }
  }
})

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const requiresSuperAdmin = to.matched.some(record => record.meta.requiresSuperAdmin)

  if (requiresAuth) {
    firebase.auth().onAuthStateChanged((user) => {
      function proceed () {
        const { getUser } = store.getters
        if (getUser && getUser.admin) {
          next()
        } else {
          next('/admin/accessdenied')
        }
      }

      if (user !== null) {
        if (requiresSuperAdmin) {
          if (store.state.user) {
            proceed()
          } else {
            store.watch(
              state => state.user,
              value => { if (value) proceed() }
            )
          }
        } else {
          next()
        }
      } else {
        next('/login')
      }
    })
  } else next()
})

new Vue({
  // el: '#app',
  router,
  render: h => h(App),
  store
}).$mount('#app')
