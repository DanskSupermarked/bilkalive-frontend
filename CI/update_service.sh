#!/bin/bash

usage(){
        echo "Usage: $0 [-e environment] [-i image version]" >&2
        exit 1
}

# Parsing command line parameters
while getopts n:p:e:h:i: OPT; do
    case "$OPT" in
      e)
        ENV="$OPTARG" ;;
      i)
        BUILD_NUMBER="$OPTARG" ;;
      \?)
        usage
        ;;
      :)
        echo "Option -$OPTARG requires an argument." >&2
        exit 1
      ;;
    esac
done

shift $(($OPTIND-1))

if [ ! "$ENV" ] || [ ! "$BUILD_NUMBER" ]
then
    usage
fi

SERVICE_NAME="${ENV}-BilkaLive-web"
IMAGE_VERSION="${ENV}_${BUILD_NUMBER}"
TASK_FAMILY="${ENV}-BilkaLiveweb"

# Create a new task definition for this build
sed -e "s;%BUILD_NUMBER%;${BUILD_NUMBER};g" ${ENV}-bilkaliveweb.json > ${ENV}-bilkaliveweb-v_${BUILD_NUMBER}.json
aws ecs register-task-definition --cli-input-json file://${ENV}-bilkaliveweb-v_${BUILD_NUMBER}.json

# Update the service with the new task definition and desired count
TASK_REVISION=`aws ecs describe-task-definition --task-definition ${TASK_FAMILY} | egrep "revision" | tr "/" " " | awk '{print $2}' | sed 's/"$//'`
DESIRED_COUNT=`aws ecs describe-services --cluster BilkaLive --services ${SERVICE_NAME} | egrep "desiredCount" | head -n 1 | awk '{print $2}' | sed 's/,$//'`
if [ ${DESIRED_COUNT} = "0" ]; then
    DESIRED_COUNT="1"
fi

aws ecs update-service --cluster BilkaLive --service ${SERVICE_NAME} --task-definition ${TASK_FAMILY}:${TASK_REVISION} --desired-count ${DESIRED_COUNT}

