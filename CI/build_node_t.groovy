node {

        stage('Cleanup Workspace') {
            step([$class: 'WsCleanup'])
        }
        

        stage('Checkout Bilka-Frontend Repository') {
            dir('bilkalive-frontend') {
                def scmFrontend = checkout([$class: 'GitSCM',
                    branches: [[name: env.bilkalive_frontend_commit_id]],
                    doGenerateSubmoduleConfigurations: false,
                    extensions: [[$class: 'SubmoduleOption', disableSubmodules: false, parentCredentials: true, recursiveSubmodules: true, reference: '', trackingSubmodules: false]],
                    submoduleCfg: [],
                    userRemoteConfigs: [[credentialsId: 'ebfd7687-112c-4870-ac3a-aefcd8cddbab',
                    url: 'git@bitbucket.org:DanskSupermarked/bilkalive-frontend.git']]])
                env.GIT_COMMIT = scmFrontend.GIT_COMMIT
            }
        }
        stage('Building version.txt for BilkaliveWeb') {
            sh 'echo DEPLOY_TIME=$BUILD_TIMESTAMP > bilkalive-frontend/public/version.txt'
            sh 'echo BUILD_URL=$BUILD_URL >> bilkalive-frontend/public/version.txt'
            sh 'echo "*****************************" >> bilkalive-frontend/public/version.txt'
            sh 'echo "BilkaLiveFrontend:" >> bilkalive-frontend/public/version.txt'
            sh 'echo GIT_COMMIT=$GIT_COMMIT >> bilkalive-frontend/public/version.txt'
        }
        stage('Build Container') {
            sh 'cp bilkalive-frontend/CI/Dockerfile .'
            sh 'bilkalive-frontend/CI/build_container_p_aws.sh -n t-BilkaLiveweb -e t -i ${BUILD_NUMBER}'
        }
        stage('Create a new task definition and update ECS bifrost service') {
            sh 'cd bilkalive-frontend/CI;./update_service.sh -e t -i ${BUILD_NUMBER}'
        }
}
