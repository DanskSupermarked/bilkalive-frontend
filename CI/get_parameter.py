#!/usr/bin/python3

import sys
import boto3

def getParameter(param_name):
    # Create the SSM Client
    ssm = boto3.client('ssm',
        region_name='eu-west-1'
    )

    # Get the requested parameter
    response = ssm.get_parameters(
        Names=[
            param_name,
        ],
        WithDecryption=False
    )

    # Store the credentials in a variable
    secret = response['Parameters'][0]['Value']
    return secret


if __name__ == '__main__':
    if len(sys.argv) == 3  and (sys.argv[1] == '--parameter'):
        config = getParameter(sys.argv[2])
    else:
        print("Usage: %s --parameter <parameter>" % (sys.argv[0]))
        sys.exit(1)
    print(config)

