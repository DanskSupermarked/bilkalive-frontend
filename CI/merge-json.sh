#!/bin/bash

usage(){
        echo "Usage: $0 [-e environment]" >&2
        exit 1
}

# Parsing command line parameters
while getopts e: OPT; do
    case "$OPT" in
      e)
        ENV="$OPTARG" ;;
      \?)
        usage
        ;;
      :)
        echo "Option -$OPTARG requires an argument." >&2
        exit 1
      ;;
    esac
done

shift $(($OPTIND-1))

if [ ! "$ENV" ]
then
    usage
fi

jq -n 'reduce inputs as $i ({}; . * $i)' bilkalive-frontend/${ENV}-env.json /tmp/${ENV}-bilkalive.json > /tmp/${ENV}-env.json ; cp -r /tmp/${ENV}-env.json bilkalive-backend/${ENV}-env.json

