#!/bin/bash

usage(){
        echo "Usage: $0 [-n container name] [-e environment] [-i image version]" >&2
        exit 1
}

# Parsing command line parameters
while getopts n:e:i: OPT; do
    case "$OPT" in
      n)
        CONTAINER="$OPTARG" ;;
      e)
        ENV="$OPTARG" ;;
      i)
        IMAGE_VERSION="$OPTARG" ;;
      \?)
        usage
        ;;
      :)
        echo "Option -$OPTARG requires an argument." >&2
        exit 1
      ;;
    esac
done

shift $(($OPTIND-1))

if [ ! "$CONTAINER" ] || [ ! "$ENV" ] || [ ! "$IMAGE_VERSION" ]
then
    usage
fi

# Build the container
docker build -t 886770918800.dkr.ecr.eu-west-1.amazonaws.com/bilkaliveweb:${ENV}_${IMAGE_VERSION} .
if [ $? -ne 0 ]
then
   echo "An error occurred during image building."
   exit 2
fi

# Push image
docker push 886770918800.dkr.ecr.eu-west-1.amazonaws.com/bilkaliveweb:${ENV}_${IMAGE_VERSION}
if [ $? -ne 0 ]
then
   echo "An error occurred during image pushing."
   exit 2
fi

# Remove image from local registry
docker rmi 886770918800.dkr.ecr.eu-west-1.amazonaws.com/bilkaliveweb:${ENV}_${IMAGE_VERSION}

# Remove unused images
docker rmi $(docker images --filter "dangling=true" -q --no-trunc) 2>/dev/null || true