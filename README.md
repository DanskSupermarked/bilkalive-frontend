# BilkaLive Frontend

## Using
* Vue CLI
* Vue.js (Vuetifyjs)

## Project setup
```
npm install
```
Setup `.env.local` based on `.env.local.example`.

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
test production on locale
```
npm install -g serve
serve -s dist
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Environments
Published development server (auto builds when pushing changes in the development branch)
https://d-bilkalive.dsgapps.dk/

Published test server
https://t-bilkalive.dsgapps.dk/

Published production server
https://p-bilkalive.dsgapps.dk/