import Vue from 'vue'
import Router from 'vue-router'
import Home from '../pages/Home'
import Event from '../pages/Event'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        }, {
            path: '/event/:slug',
            name: 'Event',
            component: Event
        }
        // , {
        //     path: '/events',
        //     name: 'Events',
        //     component: EventsPage
        // }, {
        //     path: '/events/:event_slug',
        //     name: 'EventView',
        //     component: EventViewPage
        // }, {
        //     path: '/events/:event_slug/edit',
        //     name: 'EventEdit',
        //     component: EventEditPage
        // }
    ]
})
