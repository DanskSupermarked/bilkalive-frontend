import Vue from 'vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import App from './App.vue'
import router from './router/router'
import axios from 'axios'
import 'vuetify/dist/vuetify.min.css'

Vue.use(VueRouter)
Vue.use(Vuetify)
Vue.config.productionTip = false
Vue.prototype.$http = axios

new Vue({
  //el: '#app',
  router,
  render: h => h(App),
}).$mount('#app')
